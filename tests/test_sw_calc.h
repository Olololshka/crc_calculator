//

#include <cxxtest/TestSuite.h>

#include "CRC32_calculator.h"
#include "CRC32_software.h"

class test_sw_calc : public CxxTest::TestSuite {
public:
  void testCreate() { CRC32_calculator::getSWCalculator(); }

  void testCreateSW() { CRC32_software(); }

  // mast be equalent with
  // echo -n "Test1" | python3 -c 'import sys;import zlib; \
  // print(zlib.crc32(sys.stdin.read().encode())%(1<<32))'
  void testCalc() {
    uint8_t d[] = "Test1";

    auto calc = CRC32_calculator::getSWCalculator();
    TS_ASSERT_EQUALS(1265890278, calc->accamulate(d, sizeof(d) - 1));
  }
};
