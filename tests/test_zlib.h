#ifndef TEST_ZLIB_H
#define TEST_ZLIB_H

#include <cxxtest/TestSuite.h>

#include "CRC32_calculator.h"
#include "CRC32_platform.h"

class test_sw_calc : public CxxTest::TestSuite {
public:
  void test_create() { CRC32_calculator::getPlatformCalculator(); }

  void test_createP() { CRC32_platform(); }

  void testCalc() {
    uint8_t d[] = "Test1";

    auto calc = CRC32_calculator::getPlatformCalculator();
    TS_ASSERT_EQUALS(1265890278, calc->accamulate(d, sizeof(d) - 1));
  }
};

#endif // TEST_ZLIB_H
