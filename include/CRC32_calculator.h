#ifndef _CRC32_CALCULATOR_H_
#define _CRC32_CALCULATOR_H_

#include <memory>
#include <stdint.h>
#include <vector>

struct CRC32_calculator {
  virtual uint32_t accamulate(const std::vector<uint8_t> &data) = 0;
  virtual uint32_t accamulate(const uint8_t *data, const size_t length) = 0;
  virtual void reset() = 0;

  static std::unique_ptr<CRC32_calculator> getSWCalculator();
  static std::unique_ptr<CRC32_calculator> getPlatformCalculator();
};

#endif /* _CRC32_CALCULATOR_H_ */
