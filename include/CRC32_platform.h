#ifndef _PLATFORM_CRC32_
#define _PLATFORM_CRC32_

#include "CRC32_calculator.h"

class CRC32_platform : public CRC32_calculator {
public:
  CRC32_platform();
  ~CRC32_platform();

  uint32_t accamulate(const std::vector<uint8_t> &data) override;
  uint32_t accamulate(const uint8_t *data, const size_t length) override;
  void reset() override;

private:
  void *m_context;
};

#endif /* _PLATFORM_CRC32_ */
