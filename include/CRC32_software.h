#ifndef _SOFT_CRC32_
#define _SOFT_CRC32_

#include "CRC32_calculator.h"

class CRC32_software : public CRC32_calculator {
public:
  CRC32_software();

  uint32_t accamulate(const std::vector<uint8_t> &data) override;
  virtual uint32_t accamulate(const uint8_t *data, size_t length) override;

  void reset() override;

private:
  uint32_t crc32;
};

#endif /* _SOFT_CRC32_ */
