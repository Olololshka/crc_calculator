#include "CRC32_calculator.h"

#include "CRC32_platform.h"
#include "CRC32_software.h"

std::unique_ptr<CRC32_calculator> CRC32_calculator::getSWCalculator() {
  return std::unique_ptr<CRC32_calculator>(new CRC32_software);
}

std::unique_ptr<CRC32_calculator> CRC32_calculator::getPlatformCalculator() {
  return std::unique_ptr<CRC32_calculator>(new CRC32_platform);
}
