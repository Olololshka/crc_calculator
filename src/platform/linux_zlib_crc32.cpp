#include "CRC32_platform.h"

#include <zlib.h>

/// Zlib platform realisation

CRC32_platform::CRC32_platform() : m_context(new uint32_t()) { reset(); }

CRC32_platform::~CRC32_platform() { delete m_context; }

uint32_t CRC32_platform::accamulate(const std::vector<uint8_t> &data) {
  return accamulate(data.data(), data.size());
}

uint32_t CRC32_platform::accamulate(const uint8_t *data, const size_t length) {
  return crc32(*static_cast<uint32_t *>(m_context), data, length);
}

void CRC32_platform::reset() { *static_cast<uint32_t *>(m_context) = 0; }
