#include <hw_includes.h>

#include <CRC32_platform.h>

/// STM32_CORTEX-M hardware crc32 calculator

static uint32_t calculator_count = 0;

#if defined(STM32F3)
static CRC_HandleTypeDef hcrc{CRC};
#elif defined(STM32L4)
static CRC_HandleTypeDef hcrc{CRC,
                              {DEFAULT_POLYNOMIAL_ENABLE,
                               DEFAULT_INIT_VALUE_ENABLE, 0, CRC_POLYLENGTH_32B,
                               0, CRC_INPUTDATA_INVERSION_NONE,
                               CRC_OUTPUTDATA_INVERSION_DISABLE},
                              HAL_UNLOCKED,
                              HAL_CRC_STATE_RESET,
                              CRC_INPUTDATA_FORMAT_BYTES};
#endif

static void init_crc32_module() {
  if (calculator_count == 0) {
    __HAL_RCC_CRC_CLK_ENABLE();
    HAL_CRC_Init(&hcrc);
  }
  ++calculator_count;
}

static void deinit_crc32_module() {
  --calculator_count;
  if (calculator_count <= 0) {
    HAL_CRC_DeInit(&hcrc);
    __HAL_RCC_CRC_CLK_DISABLE();
    calculator_count = 0;
  }
}

static inline uint32_t CRC_CalcCRC(uint32_t Data) {
  CRC->DR = Data;
  return CRC->DR;
}

static inline uint32_t reverse_32(uint32_t data) { return __RBIT(data); }

CRC32_platform::CRC32_platform() {
  init_crc32_module();
  reset();
}

CRC32_platform::~CRC32_platform() { deinit_crc32_module(); }

uint32_t CRC32_platform::accamulate(const std::vector<uint8_t> &data) {
  return accamulate(data.data(), data.size());
}

uint32_t CRC32_platform::accamulate(const uint8_t *data, const size_t length) {
  size_t len = length;
  uint32_t *p = (uint32_t *)data;
  uint32_t crc, crc_reg;

  if (len >= sizeof(uint32_t)) {
    while (len >= sizeof(uint32_t)) {
      crc_reg = CRC_CalcCRC(reverse_32(*p++));
      len -= sizeof(uint32_t);
    }
  } else {
    crc = 0xFFFFFFFF;
    crc_reg = CRC_CalcCRC(0xEBABAB);
  }

  crc = reverse_32(crc_reg);
  if (len) {
    CRC_CalcCRC(crc_reg);
    switch (len) {
    case 1:
      crc_reg = CRC_CalcCRC(reverse_32((*p & 0xFF) ^ crc) >> 24);
      crc = (crc >> 8) ^ reverse_32(crc_reg);
      break;
    case 2:
      crc_reg = CRC_CalcCRC(reverse_32((*p & 0xFFFF) ^ crc) >> 16);
      crc = (crc >> 16) ^ reverse_32(crc_reg);
      break;
    case 3:
      crc_reg = CRC_CalcCRC(reverse_32((*p & 0xFFFFFF) ^ crc) >> 8);
      crc = (crc >> 24) ^ reverse_32(crc_reg);
      break;
    }
  }

  return ~crc;
}

void CRC32_platform::reset() { __HAL_CRC_DR_RESET(&hcrc); }
