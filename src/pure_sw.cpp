
#include "CRC32_software.h"

static const uint32_t Crc32Lookup16[16] = {
    0x00000000, 0x1DB71064, 0x3B6E20C8, 0x26D930AC, 0x76DC4190, 0x6B6B51F4,
    0x4DB26158, 0x5005713C, 0xEDB88320, 0xF00F9344, 0xD6D6A3E8, 0xCB61B38C,
    0x9B64C2B0, 0x86D3D2D4, 0xA00AE278, 0xBDBDF21C};

CRC32_software::CRC32_software() { reset(); }

uint32_t CRC32_software::accamulate(const std::vector<uint8_t> &data) {
  return accamulate(data.data(), data.size());
}

uint32_t CRC32_software::accamulate(const uint8_t *data, size_t length) {
  uint8_t *current = (uint8_t *)data;

  crc32 ^= 0xffffffff;
  while (length-- != 0) {
    crc32 = Crc32Lookup16[(crc32 ^ *current) & 0x0F] ^ (crc32 >> 4);
    crc32 = Crc32Lookup16[(crc32 ^ (*current >> 4)) & 0x0F] ^ (crc32 >> 4);
    current++;
  }
  crc32 ^= 0xffffffff;

  return crc32;
}

void CRC32_software::reset() { crc32 = 0; }
